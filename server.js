var express = require('express');
var userFile = require('./user.json');
var bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.json());
var totalUsers = 0;
const URL_BASE = '/apitechu/v1/';
const PORT = process.env.PORT || 3000;

//Peticion GET de todos los 'users' (Collections)
app.get(URL_BASE + 'users',
      function(request, response) {
        console.log('GET ' + URL_BASE + 'users');
        response.send(userFile);
});

//Peticion GET de un 'user' (Instance)
app.get(URL_BASE + 'users/:id',
      function(request, response){
        console.log('GET ' + URL_BASE + 'users/id' );
        let indice = request.params.id;
        let instancia = userFile[indice-1];
        console.log(instancia);
      /* if (instancia != undefined)
          response.send(instancia);
        else {
          response.send({"mensaje":"Recurso no encontrado"});
        }*/
        let respuesta = (instancia != undefined) ? instancia : {"mensaje":"Recurso no encontrado"};
        response.status(200); //codigo de error HTTP
        response.send(respuesta);
        //console.log(request.params.id);
});

//Peticion GET con Query String
app.get(URL_BASE + 'usersq',
      function(request, response){
        console.log('GET ' + URL_BASE + 'con query string' );
        console.log(request.query);
        response.send(respuesta);
});

//Peticion POST
app.post(URL_BASE + 'users',
      function(req,res){
        totalUsers = userFile.length;
        cuerpo = req.body;
        console.log(req.body);
        if(cuerpo.length > 0){
        let newUser = {
          userId : totalUsers + 1,
          first_name : req.body.first_name,
          last_name : req.body.last_name,
          email : req.body.email,
          password : req.body.password
        }
        userFile.push(newUser);
        writeUserDataToFile(userFile);
        res.status(200);
        res.send({"mensaje" : "Usuario creado con exito", "usuario" : newUser});
      }else{
        res.send({"mensaje" : "Usuario creado con exito"});
      }
});

//Peticion PUT
app.put(URL_BASE + 'users/:id',
      function(req,res){
        userID = req.params.id;
        let instancia = userFile[userID-1];
       if(instancia != undefined){
         instancia.first_name = req.body.first_name;
         instancia.last_name = req.body.last_name;
         instancia.email = req.body.email;
         instancia.password = req.body.password;
         res.send(instancia);
         res.status(201);
       }else{
         res.send({"mensaje" : "Usuario no se encontro"});
       }
});

//Peticion DELETE
app.delete(URL_BASE + 'users/:id',
      function(req,res){
        userID = req.params.id;
        if ((userID != undefined) && (userFile[userID-1] != undefined))
        {
          userFile.splice(userID,1);
          console.log(userFile.length);
          res.send({"mensaje" : "Usuario " + userID + " eliminado"});
          res.status(204);
        }
        else{
          res.send({"mensaje" : "Usuario no encontrado"});
        }

})

//LOGIN
app.post(URL_BASE + 'login',
      function(req,res){
        var email = req.body.email;
        var password = req.body.password;
        for(us of userFile)
        {
          if(us.email == email)
          {
            if(us.password == password)
            {
              us.logged = true;
              writeUserDataToFile(userFile);
              res.send({"mensaje" : "Login correcto " + "idUsuario " + us.userID });
            }
            else {
              res.send({"mensaje" : "Login incorrecto"});
            }
          }
        }

});

//LOGOUT
app.post(URL_BASE + 'logout/:id',
      function(req,res){
        userID = req.params.id;
        for(us of userFile)
        {
          if(userID != undefined)
          {
            if(us.userID ==  userID)
            {
             if(us.logged)
             {
             delete us.logged;
             writeUserDataToFile(userFile);
             res.send({"mensaje" : "Logout correcto de usuario " + userID});
            }else{
             res.send({"mensaje" : "El usuario " + userID + " no se encuentra logado"})
           }
           }
          }
           else {
             res.send({"mensaje" : "Logout incorrecto"});
           }
        }
});

//Usuarios logados
app.get(URL_BASE + 'loginwho',
      function(req,res){
        var userLog = [];
        for(us of userFile)
        {
          if(us.logged)
          {
            userLog.push(us);
          }
        }
        (userLog.length > 0) ? res.send(userLog) : res.send({"mensaje" : "No existen usuarios logados"});
});

//Limitar usuarios
app.get(URL_BASE + 'usersp',
      function(req,res){
        var userQ = [];
        inf = req.query.inf;
        sup = req.query.sup;
          if (inf > 0)
          {
            if(sup <= userFile.length)
            {
              if (inf <= sup)
              {
                for(i=inf-1; i < sup; i++)
                {
                  if(userFile[i] != undefined){
                    userQ.push(userFile[i]);
                  }
                }
                (userQ.length > 0) ? res.send(userQ) : res.send({"mensaje" : "No existen usuarios"});
              }else{
                res.send({"mensaje" : "Limite inferior debe ser menor al superior"});
              }
            }else{
              res.send({"mensaje" : "Limite superior fuera de rango"});
            }
          }else{
            res.send({"mensaje" : "Limite inferior fuera de rango"})
          }
});

function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);

   fs.writeFile("./user.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'user.json'.");
      }
    });
 }

app.listen(PORT,
      function(){
        console.log('API Tech U escuchando en puerto 3000...');
});
